#!/bin/bash

index_name=$(cat conf/properties.json | jq --raw-output .name)

api_status_out=$(tempfile)
api_response_body_out=$(tempfile)

curl -XPUT -w '%{http_code}' -o $api_response_body_out -H 'Content-Type: application/json' http://localhost:9200/$index_name --data-binary @conf/index_definition.json > $api_status_out

api_proc_result=$?

if [[ !("$api_proc_result" = '0') ]]; then
	exit $api_proc_result
fi

cat $api_response_body_out | jq .
echo ''

api_status=$(cat $api_status_out)

if [[ !("$api_status" = '200' || "$api_status" = '201') ]]; then
	exit 1
fi
