#!/bin/bash

index_name=$(cat conf/properties.json | jq --raw-output .name)

curl -XDELETE http://localhost:9200/$index_name

echo ''