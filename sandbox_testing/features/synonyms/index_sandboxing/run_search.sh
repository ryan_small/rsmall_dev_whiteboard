#!/bin/bash

search_path=$1
curl -XPOST -H 'Content-Type: application/json' http://localhost:9200/analysis_sandbox/_search --data-binary @"$search_path" | jq .