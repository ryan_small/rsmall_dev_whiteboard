#!/bin/bash

index_name=$(cat conf/properties.json | jq --raw-output .name)

for doc in `ls docs`; do echo
	curl -XPUT -H 'Content-Type: application/json' http://localhost:9200/$index_name/basic/$doc -d @docs/$doc
done

echo ''
