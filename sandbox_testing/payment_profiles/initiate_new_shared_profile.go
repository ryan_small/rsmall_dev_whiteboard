package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	apis "bitbucket.org/platformdevelopment/sandbox_testing/apis"
	config "bitbucket.org/platformdevelopment/sandbox_testing/config"
	errors "bitbucket.org/platformdevelopment/sandbox_testing/errors"
	models "bitbucket.org/platformdevelopment/sandbox_testing/models"
)

const (
	newProfileInitialDataFilePath = "data/new_trustee_account_initialization.json"
)

func main() {
	config := config.NewConfig("local")

	userContext := models.UserContext{
		"DUT007",
		"VRAMISETTI",
	}
	loginCredentials := models.MediatorAuthCredentials{
		"LEMANSCORP",
		"VRAMISETTI",
		"Password1",
	}
	initiationRequest := apis.ISTNewPaymentProfileRequest{
		config.AppDev.ProfileTypes.Shared,
		userContext.Username,
	}

	apiCookieMgr := apis.NewCookieManager("")
	defer apiCookieMgr.Close()
	istLoginApi := apis.NewISTLoginApi(config, apiCookieMgr.Jar)
	istPaymentApi := apis.NewISTPaymentApi(config, apiCookieMgr.Jar)
	istLoginApi.Login(&loginCredentials)
	response := istPaymentApi.StartNewProfile(&userContext, &initiationRequest)
	log.Printf("Response from IST Payment API: %s", response)
	responseBytes, responseBytesErr := json.Marshal(response)
	errors.CheckError("Encoding Response Object", responseBytesErr)
	ioutil.WriteFile(newProfileInitialDataFilePath, responseBytes, os.ModePerm)
}
