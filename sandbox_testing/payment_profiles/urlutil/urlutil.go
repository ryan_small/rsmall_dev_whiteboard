package urlutil

import (
	"fmt"
	"net/url"
	"strings"
)

func EncodeUrlQueryParam(name string, value interface{}) string {
	return fmt.Sprintf("%s=%s", name, url.QueryEscape(value.(string)))
}

func AppendQueryStringParam(queryString string, paramName string, paramValue interface{}) string {
	paramAppendToken := ""
	if len(queryString) > 0 {
		paramAppendToken = "&"
	}
	return queryString + paramAppendToken + EncodeUrlQueryParam(paramName, paramValue)
}

func AppendQueryString(url string, queryString string) string {
	paramAppendToken := "?"
	if strings.Contains(url, paramAppendToken) {
		paramAppendToken = "&"
	}
	return url + paramAppendToken + queryString
}
