package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"github.com/akamensky/argparse"

	apis "bitbucket.org/platformdevelopment/sandbox_testing/apis"
	config "bitbucket.org/platformdevelopment/sandbox_testing/config"
	errors "bitbucket.org/platformdevelopment/sandbox_testing/errors"
	jsonutil "bitbucket.org/platformdevelopment/sandbox_testing/jsonutil"
	models "bitbucket.org/platformdevelopment/sandbox_testing/models"
)

const (
	newProfileInitialDataFilePath = "data/new_trustee_account_initialization.json"
	newProfileCreationResultPath = "data.trustee_account_creation_result.xml"
)

func main() {
	argParser := argparse.NewParser("register_dealer_api_key", "This action inserts API key for a dealer.")

	var outputNewAccountOnly *bool = argParser.Flag("o", "output-new-account-only", &argparse.Options{
		Help: "If specified, do not submit back to IST Payment API.",
	})

	argParseError := argParser.Parse(os.Args)
	errors.CheckError("CLI Argument Parse", argParseError)

	config := config.NewConfig("local")

	initialDataFile, initialDataOpenErr := os.Open(newProfileInitialDataFilePath)
	errors.CheckError("Opening initial data", initialDataOpenErr)
	initialDataBytes, initialDataBytesErr := ioutil.ReadAll(initialDataFile)
	errors.CheckError("Reading initial data bytes", initialDataBytesErr)
	initialData := models.TrustEEAccountInitalResponse{}
	jsonDecodeErr := json.Unmarshal(initialDataBytes, &initialData)
	errors.CheckError("Decoding Trustee Response", jsonDecodeErr)
	log.Printf("initialData.url=%s", initialData.Results.Url)

	userContext := models.UserContext{
		"DUT007",
		"VRAMISETTI",
	}
	userEntry := models.PaymentAccountUserEntry {
		HolderName: "Ryan Small",
		CreditCard: models.PaymentAccountCreditCardInfo {
			Number: "4111111111111111",
			MerchantType: "VS",
			ExpirationMonth: "04",
			ExpirationYear: "22",
			SecurityCode: "123",
		},
		BillingAddress: models.StreetAddress {
			Address1: "123 Test St.",
			City: "Somewhere",
			State: "CA",
			ZipCode: "90001",
			Country: "US",
		},
		ContactInfo: models.PaymentAccountContactInfo {
			PhoneNumber: "608-111-2222",
			EmailAddress: "johncocktoasten@mail.com",
		},
	}

	apiCookieMgr := apis.NewCookieManager("")
	defer apiCookieMgr.Close()
	vendorApi := apis.NewTrustEEApi(apiCookieMgr.Jar)
	istPaymentApi := apis.NewISTPaymentApi(config, apiCookieMgr.Jar)
	newAccountResult := vendorApi.SubmitNewAccountUserEntry(initialData, userEntry)
	if *outputNewAccountOnly {
		os.Stdout.Write([]byte(jsonutil.PrettyPrintJson(newAccountResult)))
	} else {
		log.Println("Saving New Account from TrustEE into IST Payment API")
		istPaymentApi.SaveNewAccount(&userContext, apis.NewISTPaymentSaveAccountRequest(&userContext, &userEntry.CreditCard, newAccountResult))
	}
}
