package apis

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/juju/persistent-cookiejar"

	config "bitbucket.org/platformdevelopment/sandbox_testing/config"
	errors "bitbucket.org/platformdevelopment/sandbox_testing/errors"
	models "bitbucket.org/platformdevelopment/sandbox_testing/models"
)

const (
	defaultCookieJarPath = "data/cookie-jar.txt"
)


type BufferedHttpBody struct {
	Buffer *bytes.Buffer
}

func (b BufferedHttpBody) Read(p []byte) (n int, err error) {
	return b.Buffer.Read(p)
}

func (b BufferedHttpBody) Close() error {
	return nil
}

func ReadStringResponse(response *http.Response) string {
	responseBodyBytes, readErr := ioutil.ReadAll(response.Body)
	errors.CheckError("Reading HTTP response body", readErr)
	return string(responseBodyBytes)
}

func ReadJsonResponse(response *http.Response, responseObj interface{}) {
	responseBodyBytes, readErr := ioutil.ReadAll(response.Body)
	errors.CheckError("Reading HTTP response body", readErr)
	unmarshalErr := json.Unmarshal(responseBodyBytes, responseObj)
	errors.CheckError("Decoding HTTP response as JSON", unmarshalErr)
}

func ReadXmlResponse(response *http.Response, responseObj interface{}) {
	responseBodyBytes, readErr := ioutil.ReadAll(response.Body)
	errors.CheckError("Reading HTTP response body", readErr)
	unmarshalErr := xml.Unmarshal(responseBodyBytes, responseObj)
	errors.CheckError("Decoding HTTP response as XML", unmarshalErr)
}

func ReadXmlData(xmlData []byte, responseObj interface{}) {
	unmarshalErr := xml.Unmarshal(xmlData, responseObj)
	errors.CheckError("Decoding byte array as XML", unmarshalErr)
}

func SanitizeBadXml(xmlString string) string {
	var removeNamePlox = regexp.MustCompile("<\\?\\w+>.+</\\?\\w+>")
	return removeNamePlox.ReplaceAllString(xmlString, "")
}

func WriteJsonBytesOrFail(valueObj interface{}) []byte {
	bytes, bytesErr := json.Marshal(valueObj)
	errors.CheckError("Writing JSON", bytesErr)
	return bytes
}

func WriteJsonStringOrEmpty(valueObj interface{}) string {
	if valueObj != nil {
		bytes, bytesErr := json.Marshal(valueObj)
		errors.CheckError("Writing JSON", bytesErr)
		return string(bytes)
	} else {
		return ""
	}
}

type ApiError struct {
	Message string
	ResponseObject interface{}
}

func (a ApiError) Error() string {
	return a.Message
}

type ISTLoginApi struct {
	config *config.Config
	client *http.Client
}

type ISTPaymentApi struct {
	config *config.Config
	client *http.Client
}

type TrustEEApi struct {
	client *http.Client
}

type ISTNewPaymentProfileRequest struct {
	ProfileTypeId int `json:"profileTypeId"`
	Username string `json:"username"`
}

type YesNoBool bool

func (v *YesNoBool) UnmarshalText(text []byte) error {
	switch strings.ToLower(string(text)) {
	default:
			*v = false
	case "y":
			*v = true
	case "n":
		*v = false
	}
	return nil
}

func (v YesNoBool) MarshalText() ([]byte, error) {
	if v {
		return []byte("y"), nil
	} else {
		return []byte("n"), nil
	}
}

type TrustEEResponse struct {
	XMLName xml.Name `xml:"trusteeresponse"`
	Error string `xml:"error"`
	Bcustomfieldlist int `xml:"bbcustomfieldlist"`
	Bcustomfield2 string `xml:"bcustomfield2"`
	Verify YesNoBool `xml:"verify"`
	Avs YesNoBool `xml:"avs"`
	Demo YesNoBool `xml:"demo"`
	Cvvstatus string `xml:"cvvstatus"` // TODO: there's also a ?cvvstatus field; resolve what it is for.
	CheckCvv YesNoBool `xml:"checkcvv"`
	Token string  `xml:"token"`
	Action string `xml:"action"`
	Returnurl string `xml:"returnurl"`

	AccountHolderName string `xml:"name"`
	CreditCardLastFour string `xml:"cc"`
	CreditCardExpiration string `xml:"exp"`
	CreditCardSecurityCode string `xml:"cvv"`
	AccountBillingAddress1 string `xml:"address1"`
	AccountBillingAddress2 string `xml:"address2"`
	AccountBillingCity string `xml:"city"`
	AccountBillingState string `xml:"state"`
	AccountBillingZipCode string `xml:"zip"`
	AccountBillingCountry string `xml:"country"`
	AccountContactEmailAddress string `xml:"email"`
	AccountContactPhone string `xml:"phone"`
}

type ISTEntityTypeResults struct {
	Results []ISTEntityType `json:"results"`
}

type ISTEntityType struct {
	Id int `json:"typeId"`
	Type string `json:"type"`
	Code string `json:"typeCode"`
	Description string `json:"typeDescr"`
	DisplayName string `json:"typeDisplayName"`
	Active bool `json:"isActive"`
}

type ISTPaymentSaveAccountRequest struct {
	Token string `json:"token"`
	Action string `json:"action"`
	Returnurl string `json:"returnurl"`
	Demo YesNoBool `json"demo"`
	Verify YesNoBool `json:"verify"`

	DealerCode string `json:"dealerCode"`
	CreditCardLastFour string `json:"cc"`
	CreditCardExpiration string `json:"exp"`
	CreditCardSecurityCode string `json:"cvv"`
	AccountHolderName string `json:"name"`
	AccountBillingAddress1 string `json:"address1"`
	AccountBillingCity string `json:"city"`
	AccountBillingState string `json:"state"`
	AccountBillingZipCode string `json:"zip"`
	AccountBillingCountry string `json:"country"`
	AccountContactEmailAddress string `json:"email"`
	AccountContactPhone string `json:"phone"`
	CreditCardTypeId int `json:"creditCardTypeId"`
}

func NewISTPaymentSaveAccountRequest(userContext *models.UserContext, creditCardInfo *models.PaymentAccountCreditCardInfo, response *TrustEEResponse) *ISTPaymentSaveAccountRequest {
	var creditCardTypeId int
	if creditCardInfo.MerchantType == "VC" {
		creditCardTypeId = 37001
	} else if creditCardInfo.MerchantType == "MC" {
		creditCardTypeId = 37002
	} else if creditCardInfo.MerchantType == "DS" {
		creditCardTypeId = 37003
	} else if creditCardInfo.MerchantType == "AX" {
		creditCardTypeId = 37004
	} else {
		log.Panicf("Unknown or Unacceptable Credit Merchant Type: %s", creditCardInfo.MerchantType)
	}
	return &ISTPaymentSaveAccountRequest {
		Token: response.Token,
		Action: response.Action,
		Returnurl: response.Returnurl,
		Demo: response.Demo,
		Verify: response.Verify,
		DealerCode: userContext.DealerCode,
		CreditCardLastFour: response.CreditCardLastFour,
		CreditCardExpiration: response.CreditCardExpiration,
		CreditCardSecurityCode: response.CreditCardSecurityCode,
		AccountHolderName: response.AccountHolderName,
		AccountBillingAddress1: response.AccountBillingAddress1,
		AccountBillingCity: response.AccountBillingCity,
		AccountBillingState: response.AccountBillingState,
		AccountBillingZipCode: response.AccountBillingZipCode,
		AccountBillingCountry: response.AccountBillingCountry,
		AccountContactEmailAddress: response.AccountContactEmailAddress,
		AccountContactPhone: response.AccountContactPhone,
		CreditCardTypeId: creditCardTypeId,
	}
}

type ISTPaymentProfile struct {
	PaymentProfileId int `json:"paymentProfileId"`
	ProfileTypeId int `json:"profileTypeId"`
	ProfileTypeCode int `json:"profileTypeCode"`
	ProfileTypeDescription int `json:"profileTypeDescr"`
	CustomerId string `json:"customerId"`
	BillingId string `json:"billingId"`
	DealerCode string `json:"dealerCode"`
	Username string `json:"userName"`
	CustomerName string `json:"customerName"`
	CreditCardTypeId string `json:"creditCardTypeId"`
	CreditCardTypeCode string `json:"creditCardTypeCode"`
	ExpirationMonth string `json:"expirationMonth"`
	ExpirationYear string `json:"expirationYear"`
}

type CookieManager struct {
	Jar *http.CookieJar
	PersistedJar *cookiejar.Jar
}

func (m *CookieManager) Close() {
	errors.CheckError("Saving API Cookies", m.PersistedJar.Save())
}

func NewCookieManager(overrideCookieJarPath string) *CookieManager {
	var cookieJarPath string
	if len(overrideCookieJarPath) > 0 {
		cookieJarPath = overrideCookieJarPath
	} else {
		cookieJarPath = defaultCookieJarPath
	}
	persistedHttpCookieJar, persistedHttpCookieJarErr := cookiejar.New(&cookiejar.Options {
		Filename: cookieJarPath,
		NoPersist: false,
	})
	errors.CheckError("HTTP Cookie Jar", persistedHttpCookieJarErr)
	var httpCookieJar http.CookieJar
	httpCookieJar = persistedHttpCookieJar
	return &CookieManager{&httpCookieJar, persistedHttpCookieJar}
}

func NewHttpClient(cookieJar *http.CookieJar) *http.Client {
	return &http.Client {
		Jar: *cookieJar,
	}
}

func NewTrustEEApi(cookieJar *http.CookieJar) *TrustEEApi {
	return &TrustEEApi{ NewHttpClient(cookieJar) }
}

func NewISTLoginApi(config *config.Config, cookieJar *http.CookieJar) *ISTLoginApi {
	return &ISTLoginApi{ config, NewHttpClient(cookieJar) }
}

func NewISTPaymentApi(config *config.Config, cookieJar *http.CookieJar) *ISTPaymentApi {
	return &ISTPaymentApi{ config, NewHttpClient(cookieJar) }
}

func IsStandardOkStatus(response *http.Response) bool {
	return response.StatusCode >= 200 && response.StatusCode < 300
}

func CheckResponseStatus(request *http.Request, response *http.Response, checkIsOkayStatus func(*http.Response) bool) {
	if !checkIsOkayStatus(response) {
		log.Fatalf("Request to %s[%s] Failed: Status Code %s", request.Method, request.URL, response.StatusCode)
	}
}

func ExecuteApiFor(client *http.Client, request *http.Request, handleResponse func(*http.Response) *ApiError) {
	log.Printf("Executing API: %s[%s]", request.Method, request.URL)
	apiResponse, apiExecErr := client.Do(request)
	errors.CheckError(fmt.Sprintf("Request to %s[%s]", request.Method, request.URL), apiExecErr)
	defer apiResponse.Body.Close()
	apiError := handleResponse(apiResponse)
	if apiError != nil {
		log.Panicf("API Failed: %s[%s]; error=%s; responseObject=%s", request.Method, request.URL, apiError.Message, WriteJsonStringOrEmpty(apiError.ResponseObject))
	}
}


func (a *ISTLoginApi) Login(credentials *models.MediatorAuthCredentials) {
	requestUrl, requestUrlErr := url.Parse(fmt.Sprintf(
		"%s/login?dm=%d&dealerCode=%s&userName=%s&password=%s&rememberMe=1",
		a.config.AppDev.MediatorRootUri,
		a.config.AppDev.Domain,
		credentials.DealerCode,
		credentials.Username,
		credentials.Password,
	))
	errors.CheckError("New Profile URL", requestUrlErr)
	httpRequest := http.Request {
		Method: "POST",
		URL: requestUrl,
		Header: map[string][]string{},
	}

	ExecuteApiFor(a.client, &httpRequest, func(httpResponse *http.Response) *ApiError {
		CheckResponseStatus(&httpRequest, httpResponse, IsStandardOkStatus)
		return nil
	})
}

func (a *ISTPaymentApi) StartNewProfile(userContext *models.UserContext, request *ISTNewPaymentProfileRequest) *models.TrustEEAccountInitalResponse {
	requestUrl, requestUrlErr := url.Parse(fmt.Sprintf(
		"%s/api/v1/dm/%d/dealer/%s/payment/profile/token",
		a.config.AppDev.MediatorRootUri,
		a.config.AppDev.Domain,
		userContext.DealerCode,
	))
	errors.CheckError("New Profile URL", requestUrlErr)
	httpRequest := http.Request {
		Method: "POST",
		URL: requestUrl,
		Body: BufferedHttpBody{bytes.NewBuffer(WriteJsonBytesOrFail(request))},
		Header: map[string][]string{},
	}

	var response models.TrustEEAccountInitalResponse

	ExecuteApiFor(a.client, &httpRequest, func(httpResponse *http.Response) *ApiError {
		CheckResponseStatus(&httpRequest, httpResponse, IsStandardOkStatus)
		ReadJsonResponse(httpResponse, &response)
		return nil
	})

	return &response
}

func (a *ISTPaymentApi) FormatMediatorApiBaseUrl() string {
	return fmt.Sprintf(
		"%s/api/v1/dm/%s",
		a.config.AppDev.MediatorRootUri,
		a.config.AppDev.Domain,
	)
}

func (a *ISTPaymentApi) ListCreditCardTypes() *ISTEntityTypeResults {
	requestUrl, requestUrlErr := url.Parse(fmt.Sprintf(
		"%s/api/v1/dm/%d/xref/type/CreditCard",
		a.config.AppDev.MediatorRootUri,
		a.config.AppDev.Domain,
	))
	errors.CheckError("New Profile URL", requestUrlErr)

	var responseObject ISTEntityTypeResults

	httpRequest := http.Request {
		Method: "GET",
		URL: requestUrl,
		Header: map[string][]string{},
	}

	ExecuteApiFor(a.client, &httpRequest, func(httpResponse *http.Response) *ApiError {
		CheckResponseStatus(&httpRequest, httpResponse, IsStandardOkStatus)
		ReadJsonResponse(httpResponse, &responseObject)
		return nil
	})

	return &responseObject
}

func (a *TrustEEApi) SubmitNewAccountUserEntry(paymentAccount models.TrustEEAccountInitalResponse, userEntry models.PaymentAccountUserEntry) *TrustEEResponse {
	submitUrl, submitUrlErr := url.Parse(paymentAccount.BuildFullSubmitUrl(userEntry))
	errors.CheckError("Submit URL is invalid", submitUrlErr)
	log.Printf("Submit URL: %s", submitUrl)
	request := http.Request {
		Method: "POST",
		URL: submitUrl,
	}
	responseObject := TrustEEResponse{}
	ExecuteApiFor(a.client, &request, func(response *http.Response) *ApiError {
		CheckResponseStatus(&request, response, IsStandardOkStatus)
		ReadXmlResponse(response, &responseObject)
		if len(responseObject.Error) > 0 {
			return &ApiError{responseObject.Error, responseObject}
		} else {
			return nil
		}
	})
	return &responseObject
}

func (a *ISTPaymentApi) SaveNewAccount(userContext *models.UserContext, request *ISTPaymentSaveAccountRequest) *ISTPaymentProfile {
	requestUrl, requestUrlErr := url.Parse(fmt.Sprintf(
		"%s/api/v1/dm/%d/dealer/%s/payment/profile",
		a.config.AppDev.MediatorRootUri,
		a.config.AppDev.Domain,
		userContext.DealerCode,
	))
	errors.CheckError("New Profile URL", requestUrlErr)
	httpRequest := http.Request {
		Method: "POST",
		URL: requestUrl,
		Body: BufferedHttpBody{bytes.NewBuffer(WriteJsonBytesOrFail(request))},
		Header: map[string][]string {
			"Content-Type": []string{"application/json"},
		},
	}

	var response ISTPaymentProfile

	ExecuteApiFor(a.client, &httpRequest, func(httpResponse *http.Response) *ApiError {
		CheckResponseStatus(&httpRequest, httpResponse, IsStandardOkStatus)
		ReadJsonResponse(httpResponse, &response)
		return nil
	})

	return &response
}
