package jsonutil

import (
	"encoding/json"
	
	errors "bitbucket.org/platformdevelopment/sandbox_testing/errors"
)

func PrettyPrintJson(value interface{}) string {
	encodedBytes, encodingErr := json.Marshal(value)
	errors.CheckError("Encoding Object", encodingErr)
	return string(encodedBytes)
}