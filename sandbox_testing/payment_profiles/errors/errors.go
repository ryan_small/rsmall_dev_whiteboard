package errors

import (
	"log"
)

func CheckError(situationDescription string, err error) {
	if err != nil {
		log.Panicf("Failure during %s; error: %s", situationDescription, err)
	}
}
