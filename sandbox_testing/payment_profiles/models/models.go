package models

import (
	"fmt"
	urlutil "bitbucket.org/platformdevelopment/sandbox_testing/urlutil"
)

type UserContext struct {
	DealerCode string
	Username string
}
type MediatorAuthCredentials struct {
	DealerCode string
	Username string
	Password string
}

type TrustEEAccountInitalResponse struct {
	Results TrustEEAccountInitalData `json:"results"`
}

type TrustEEAccountInitalData struct {
	Url string `json:"url"`
	Params map[string]interface{} `json:"params"`
}

type PaymentAccountUserEntry struct {
	HolderName string
	CreditCard PaymentAccountCreditCardInfo
	BillingAddress StreetAddress
	ContactInfo PaymentAccountContactInfo
}

type PaymentAccountCreditCardInfo struct {
	Number string
	MerchantType string
	ExpirationMonth string
	ExpirationYear string
	SecurityCode string
}

type PaymentAccountContactInfo struct {
	PhoneNumber string
	EmailAddress string
}

func (i PaymentAccountCreditCardInfo) Expiration() string {
	return fmt.Sprintf("%s%s", i.ExpirationMonth, i.ExpirationYear)
}

type StreetAddress struct {
	Address1 string
	Address2 string
	City string
	State string
	ZipCode string
	Country string
}

func (r *TrustEEAccountInitalResponse) BuildSubmitQueryParams(userEntry PaymentAccountUserEntry) string {
	queryString := ""
	for paramName, paramValue := range r.Results.Params {
		queryString = urlutil.AppendQueryStringParam(queryString, paramName, paramValue)
	}
	queryString = urlutil.AppendQueryStringParam(queryString, "name", userEntry.HolderName)
	queryString = urlutil.AppendQueryStringParam(queryString, "cc", userEntry.CreditCard.Number)
	queryString = urlutil.AppendQueryStringParam(queryString, "exp", userEntry.CreditCard.Expiration())
	queryString = urlutil.AppendQueryStringParam(queryString, "cvv", userEntry.CreditCard.SecurityCode)
	queryString = urlutil.AppendQueryStringParam(queryString, "cvvstatus", "present")
	queryString = urlutil.AppendQueryStringParam(queryString, "address1", userEntry.BillingAddress.Address1)
	queryString = urlutil.AppendQueryStringParam(queryString, "address2", userEntry.BillingAddress.Address2)
	queryString = urlutil.AppendQueryStringParam(queryString, "city", userEntry.BillingAddress.City)
	queryString = urlutil.AppendQueryStringParam(queryString, "state", userEntry.BillingAddress.State)
	queryString = urlutil.AppendQueryStringParam(queryString, "zip", userEntry.BillingAddress.ZipCode)
	queryString = urlutil.AppendQueryStringParam(queryString, "country", userEntry.BillingAddress.Country)
	queryString = urlutil.AppendQueryStringParam(queryString, "phone", userEntry.ContactInfo.PhoneNumber)
	queryString = urlutil.AppendQueryStringParam(queryString, "email", userEntry.ContactInfo.EmailAddress)
	return queryString
}

func (r *TrustEEAccountInitalResponse) BuildFullSubmitUrl(userEntry PaymentAccountUserEntry) string {
	return urlutil.AppendQueryString(r.Results.Url, r.BuildSubmitQueryParams(userEntry))
}
