package main

import (
	"log"

	apis "bitbucket.org/platformdevelopment/sandbox_testing/apis"
	config "bitbucket.org/platformdevelopment/sandbox_testing/config"
	jsonutil "bitbucket.org/platformdevelopment/sandbox_testing/jsonutil"
	models "bitbucket.org/platformdevelopment/sandbox_testing/models"
)

func main() {
	config := config.NewConfig("local")

	loginCredentials := models.MediatorAuthCredentials{
		"LEMANSCORP",
		"VRAMISETTI",
		"Password1",
	}

	apiCookieMgr := apis.NewCookieManager("")
	istLoginApi := apis.NewISTLoginApi(config, apiCookieMgr.Jar)
	istLoginApi.Login(&loginCredentials)
	istPaymentApi := apis.NewISTPaymentApi(config, apiCookieMgr.Jar)
	cardTypesResult := istPaymentApi.ListCreditCardTypes()
	log.Printf("Credit Card Types")
	for _, cardType := range cardTypesResult.Results {
		log.Println(jsonutil.PrettyPrintJson(cardType))
	}
}