package main

import (
	"log"
	"regexp"
	apis "bitbucket.org/platformdevelopment/sandbox_testing/apis"
)

func main() {
	var xmlString = `<?xml version="1.0"?><trusteeresponse><action>store</action><returnurl>xml</returnurl><verify>y</verify><demo>y</demo><avs>y</avs><checkcvv>y</checkcvv><cvvstatus>present</cvvstatus><bcustomfield2>S</bcustomfield2><bcustomfieldlist>2</bcustomfieldlist><token>ODA5ZWY3YmQ0ZjhhMjcyNzcxMDI4ODQ0YzUxZjFjZWM=</token><?name>Ryan Small</?name><email>johncocktoasten@mail.com</email></trusteeresponse>`
	var removeNamePlox = regexp.MustCompile("<\\?\\w+>.+</\\?\\w+>")
	xmlString = removeNamePlox.ReplaceAllString(xmlString, "")
	log.Printf("Yo dawg: %s", xmlString)
	responseObject := apis.TrustEEResponse{}
	apis.ReadXmlData([]byte(xmlString), &responseObject)
}
