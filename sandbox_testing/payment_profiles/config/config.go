package config

import (
	"fmt"

	"github.com/spf13/viper"

	errors "bitbucket.org/platformdevelopment/sandbox_testing/errors"
)

type Config struct {
	AppDev AppDev `json:"appDev"`
	PaymentVendor PaymentVendor `json:"paymentVendor"`
}
type AppDev struct {
	MediatorRootUri string `json:"mediatorRootUri"`
	Domain int `json:"domain"`
	DomainParamName string `json:"domainParamName"`
	ProfileTypes ProfileTypes `json:"profileTypes"`
}
type PaymentVendor struct {
	RootUri string `json:"rootUri"`
}
type ProfileTypes struct {
	Personal int `json:"personal"`
	Shared int `json:"shared"`
}

func NewConfig(envName string) *Config {
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath(fmt.Sprintf("env/%s", envName))
	errors.CheckError("Reading Config", viper.ReadInConfig())
	var config Config
	viper.Unmarshal(&config)
	return &config
}
