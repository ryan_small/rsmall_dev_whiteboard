
1. Determine what good candidates for processing extracted DIGI datasets
   - relational model creates a deep enough object hierarchy/graph that the datasets
     will need to be indexed by relational keys like product ids, part numbers, brand ids, etc.
2. Indexing performance; try indexing deeper objects like products pieces at a time.
3. Refacter out common pieces as recent requests for ETL changes have seen a couple
   of cases where
4. SQL transaction isolation when extracting from live, locking systems
	https://docs.microsoft.com/en-us/previous-versions/sql/sql-server-2008-r2/ms187101(v=sql.105)
	
TODO: 
	Remove log line in product_service: Injecting quotes into part

SLA/UAT
	Data Schema
		- current repo state of ES doc schema has to be validated as
			1. workable with current data ie. could you reindex with it
			2. workable with importer either in RC repo or deployed to
			   the env.
		- compare against code class definition
		- possible change: derive/generate ES doc schema from class model and
		  annotations on it with JsonViews and analyzer/similarity sub-field
		  capability

