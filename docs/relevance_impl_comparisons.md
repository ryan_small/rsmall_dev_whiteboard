denorm-merch >> digishadow3
  CAVEAT:
  	- locally i switched term joins in the lucene boolean from AND to OR
  	- first page has 25 results

  great
  	street riding pants
	2-into-1 (although "2 into 1" does not work .. can remedy)
  	hi viz jacket
  	hi viz helmet
    blue helmet
    red helmet
    youth jersey
    black boots
    dunlop tire
    backpack

  alright
  	legends shock
  		- top 6 are legends suspensions
  		- there is legends x3000 helmets, legends gloves in top 15

  	textile jacket
  		- most results are spot-on
  		- there are a couple of pants showing up

  	tech 7
  		- enough of results are alpinestart tech boots but many up top are not tech 7

	icon boots
		- about 5-7 non icon-branded results show up in first page and 3 of top 10.

  	racing line
  		- some Akrapovic "Racing Line"-branded products show up, but most results are unrelated.

  	socks
  		- most results are socks but the fuzz is bringing things in like "windsock"

  	black and white three quarter helmets
  		- a lot of top results are not 3/4 helmets
  		- otherwise alright

  	leather vests (or "leather vest")
  		- approx quarter of first page are not vests

  	magnaflow
  		- about half of first page are not magnaflow brand parts
  		- most of top 10 are magnaflow (7/10)

  bad
  	moose racing
  		- most of first page are not moose racing brand parts

  	heavy duty ratcheting tie downs
  		- things like https://stage.lemansplatform.com/product/parts-unlimited-1-heavy-duty-ratcheting-tie-downs/1727170292 are too far down.

  	2 stroke oil
  		- 4-stroke oils show up top for the most part
  		- non oils show up after a few 2-stroke oils show up

  	iridium ix
  		- shows up too far down

    243 gloves
    	- the 243 gloves are not up top; completely unrelated around.

	naked shield
		- feedback a long time ago, user expected this product:
			- https://stage.lemansplatform.com/product/agv-ax-8-ds-shield/1668747571
			- it's because it lists the AX-8 EVO Naked helmet under "Model" attribute

	xc1 base
		- expecting products with phrase "xc1 base"
		- ex: https://stage.lemansplatform.com/product/moose-racing-soft-goods-xc1-base-jersey/0231157569

	acorn front belt
		- a product by this name exists but digishadow3 does not even show it until 12th result

	motion pro tech
		- motion pro tech gloves is nowhere near the top

  	forged shift lever
  		- most results are not like https://stage.lemansplatform.com/search;q=forged%20shift%20lever;r=eJyrVkrLzClJLSpWsoqOrQUAJusFKA%3D%3D
  		- most are something other than a shift lever

  postponed

  	apparel
  		- results are not apparel but ..
  		- current implmentation does not have ancestor categories and apparel is a non-leaf category
  		  so text match is not possible here.

    make/model phrases since current impl does not have them.

