

icon leather jacket

-> 
    1. [icon, leather, jacket]
    2. ["icon leather", jacket]
    3. [icon, "leather jacket"]
    4. ["icon leather jacket"]


[icon, leather, jacket, "iconleather", "leatherjacket", "iconleatherjacket"]

{ "query": { "query_string": { "query": "icon", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] }}}
{ "query": { "query_string": { "query": "leather", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] }}}
{ "query": { "query_string": { "query": "jacket", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] }}}
{ "query": { "query_string": { "query": "iconleather", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] }}}
{ "query": { "query_string": { "query": "leatherjacket", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] }}}
{ "query": { "query_string": { "query": "iconleatherjacket", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] }}}






curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "icon", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "leather", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "jacket", "fields": ["matchTerm.tokens.english_stems_ib", "matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "iconleather", "fields": ["matchTerm.tokens.search_compounds_ib"] } },{"query_string": {"query": "icon leather", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "leather jacket", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "leatherjacket", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "icon leather jacket", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "iconleatherjacket", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
' | jq . | more

curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "honda", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "gold wing", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "goldwing", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "brake", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "pad", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"term": {"classifierName": "product.name"} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
' | jq . | more


```
curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "honda", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 } {} {"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "gold wing", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "goldwing", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"]} } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "gold wing", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "goldwing", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "brake", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"]} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "pad", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
' | jq . | more
```

```
curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "honda", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 } {} {"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "gold wing", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "goldwing", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"]} } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "goldwing", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "brake", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"]} } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "pad", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "_source": {"include": ["classifierName", "classificationTarget", "classifyProbability"] }, "sort": {"classifyProbability": "desc"}, "size": 100 }
' | jq . | more
```


```
curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "honda", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }{}
{}
{"query": {"bool": {"must": {"query_string": {"query": "goldwing", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "brake", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "pad", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
' | jq . | more
```


```
curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "iridium", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }{}
{}
{"query": {"bool": {"must": {"query_string": {"query": "spark", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "plug", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
' | jq . | more
```


```
curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"query": {"bool": {"must": {"query_string": {"query": "tech", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }{}
{}
{"query": {"bool": {"must": {"query_string": {"query": "7", "fields": ["matchTerm.tokens.search_compounds_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
{}
{"query": {"bool": {"must": {"dis_max": {"queries": [{"query_string": {"query": "tech 7", "fields": ["matchTerm.tokens.english_stems_ib"], "type": "phrase"} }, {"query_string": {"query": "tech7", "fields": ["matchTerm.tokens.search_compounds_ib"] } } ] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
{}
{"query": {"bool": {"must": {"query_string": {"query": "boots", "fields": ["matchTerm.tokens.english_stems_ib"] } }, "must_not": {"terms": {"classifierName": ["product.name", "model.name"] } } } }, "aggregations": {"classifiers": {"terms": {"field": "classifierName"}, "aggregations": {"targets": {"terms": {"field": "classificationTarget"} } } } }, "size": 0 }
' | jq . | more
```

```
curl -XPOST -H 'Content-Type: application/x-ndjson' http://localhost:9200/text_classifieds/_msearch --data-binary '
{}
{"size":300,"query":{"bool":{"must":[{"dis_max":{"tie_breaker":0.0,"queries":[{"match_phrase":{"matchTerm.tokens.search_compounds_ib":{"query":"icon leather jacket","slop":0,"boost":1.0}}},{"match":{"matchTerm.tokens.search_compounds_ib":{"query":"iconleatherjacket","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"boost":1.0}}],"must_not":[{"terms":{"classificationName":["product.name","model.name"],"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
{}
{"size":300,"query":{"bool":{"must":[{"match":{"matchTerm.tokens.search_compounds_ib":{"query":"icon","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"must_not":[{"terms":{"classificationName":["product.name","model.name"],"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
{}
{"size":300,"query":{"bool":{"must":[{"dis_max":{"tie_breaker":0.0,"queries":[{"match_phrase":{"matchTerm.tokens.search_compounds_ib":{"query":"leather jacket","slop":0,"boost":1.0}}},{"match":{"matchTerm.tokens.search_compounds_ib":{"query":"leatherjacket","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"boost":1.0}}],"must_not":[{"terms":{"classificationName":["product.name","model.name"],"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
{}
{"size":300,"query":{"bool":{"must":[{"dis_max":{"tie_breaker":0.0,"queries":[{"match_phrase":{"matchTerm.tokens.search_compounds_ib":{"query":"icon leather","slop":0,"boost":1.0}}},{"match":{"matchTerm.tokens.search_compounds_ib":{"query":"iconleather","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"boost":1.0}}],"must_not":[{"terms":{"classificationName":["product.name","model.name"],"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
{}
{"size":300,"query":{"bool":{"must":[{"match":{"matchTerm.tokens.search_compounds_ib":{"query":"jacket","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"must_not":[{"terms":{"classificationName":["product.name","model.name"],"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
{}
{"size":300,"query":{"bool":{"must":[{"match":{"matchTerm.tokens.search_compounds_ib":{"query":"leather","operator":"OR","prefix_length":0,"max_expansions":50,"fuzzy_transpositions":true,"lenient":false,"zero_terms_query":"NONE","auto_generate_synonyms_phrase_query":true,"boost":1.0}}}],"must_not":[{"terms":{"classificationName":["product.name","model.name"],"boost":1.0}}],"adjust_pure_negative":true,"boost":1.0}}}
' | jq . | more
```



Product/Part pre-scalar/full text not requiring join/nested assocation:
    product.name
    part.description
    part.partNumber (irrelevant as part number lookup handled specifically but can be incorporated into this topic)
    part.productType
        .major
        .minor

Part merchandise text filter scalar terms

{
    brand: {
        id: <uuid-string>
    },
    attributeIds: [
        <verbatim(name:value)>
    ],
    categoryIds: [
        <uuid-string>
    ],
    categoryAncestorIds: [
        <uuid-string>
    ],
    vehicles: [{
      makeId: <uuid-string>,
      modelIds: <uuid-string>,
      modelSegmentIds: <verbatim(name:value)>
    }]
}


