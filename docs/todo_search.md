
# High-Level Tasks #

1. Improve some failed queries: "icon boot" when logged-in.
2. Add more test cases
3. Improve the scoring in general without breaking the ample test cases.


# Search QSA Cases #

1. "le pera"

Tackle `singleword_stems` for non-vehicle fields as latter can do this with an ES-external tokenization
for vehicle model names.  Resulting ES field for `brand.name.english_stems` will be tokenized normally with `["le", "pera"]` but the `singleword_stems` form will have one token: ["lepera"]. Search can then search on both
`brand.name.english_stems` and `brand.name.singleword_stems`.

 "cbr 1000 ff"

 "cbr1000ff"


 "alpinestar"
 "alpine jacket"
 "alpine jacket"

 icon1000

 "teeshirt"
 	- solve with synt