
<FOR RELEASE - End-of-September> Full-Text Relevance Key Metrics

	1. How relatable are match results to what's in your mind when you enter your text

	2. Will user filters correspond to what they STRICTLY want results to conform to
		eg. if you have a category filter, are the results going to be parts/products/merch.
		    ONLY be from that category.

Whole Part Number
	

Partial Part Number
	
	Is it a short-circuit?
	Is it a pre-search whose response suggests to UI that user input are partial P/Ns?

Tire-related
	
	Free/Keyword text need to handle <outer-diameter>/<width>+<rim-diameter>

Vehicle Full-Text Relevance
	
	Match user input on make models

Vehicle Auto-Filter
	
	Match user input on make models

Broad Auto-Filtering
	
	Category, Brand, Attributes, et al.