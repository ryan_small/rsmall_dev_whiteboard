

# TODO #

`hydraulic clutch` - phrasing overboosting on product name
    - "Hydraulic" is an "Actuator" attribute of parts
    - expected results are not far from top,  but the results are not matching
    PNW expectations.

`wild 1 hooked` - DIGI substitutes a marketing description into the internal part description.

`jeans` - now that we alias/synonymize "jeans" to "denim" it loses the context of pants ie. if I think if jeans
I primarily think of pants.

- for this example, the parts in DIGI have "HOOKED" in description, but the marketing description
does not have it.

Example Part Number #0601-4680; in DIGI the Product is https://stage.lemansplatform.com/product/wild-1-1-14-chubby-handlebar/0559035846/06014680

Part number search does not work with other terms:
    <normal-terms> .. <part-number>
    `velocity 21061008`

- complete depunctation char filter and notice regressions alongside improvements.
    - currently, '&', '-', and '.' are only stripped.
        - what are ramifications for removing '/' and other punctuation?


`airflite skull` - TODO: product not visible but part is!!!


`red jacket` TODO: the red sku for the merc crusader jacket is not visible; part nested query must filter out
             parts not visible


"s&s 60th" - FIXED(most results are not even S&S brand products/parts.)
    - trim the 'th' from 60th when searching.


De-emphasize Phrases when individual words match diff dimensions better
    - Examples:
        `K&N AIR FILTER`: product name with that phrase pops-up first even when not K&N brand
        `street helmet`
    - preserve examples like `Road Rage` while fixing `Icon Boot`

Extra Credit

## FIXED ##

`atv battery` - FIXED(non-batteries up top)

`FENDER RR FLS +4" W/FRNCH` - this exact part description matches only two parts now.
    - should match `1401-0580` exactly.

`2018 fender skin xlcx` - find 1405-0141 or others

`r1 stator` - finds 2112-0012

`"2-3/4 cable vinyl`

`atv lighting` - DPI-1538
- in psa, light kits come back
- in platform, different results come back based on "light" vs "lighting".
    - Light product name comes back higher when searching "lighting" vs "lights" which
    is counter-intuitive.

## IDEAS ##

### Content RE-Interpretation ###

1. For a search on `hydraulic clutch`, would a user expect this as a result even secondary?
    http://int01.lemansplatform.com/product/magura-hymec-off-road-hydraulic-clutch-system/1903003313

They want a clutch, and this is a lever, but it is a kit whose primary product within is the lever, but a hydraulic clutch
is among this kit.

2. `jeans` - expand query with boosting filters on productType = `Pants|Pant` and generalAttributeValue = `Denim`

This solves that user expects primary (or only) results to be denim pants.

### Data Normalization ###

1. Could solve naturally compound words like "hogtunes" with https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-compound-word-tokenfilter.html

    - phrase matching no longer has to munge words together at query time

        - benefit is we can just use the match phrase query which has proper boosting per word length.

2. Normalize out common nouns from brands so that queries like `2013 slim parts` does not boost up
brand like "Eastern Motorcycle Parts".


### Hit Result Messaging/Highlighting ###

1. For each hit, get aggegration terms on fields of merchandise that match terms to report
in each hit
    - For Example, aggregate on model names but filter names on query `yzf650 gaskets` such that
    the per product id top-level bucket, there will be model name term buckets that will hae

2. If fuzziness/completion is turned on indicate that in search results

- if there are no hits on close/precise keyword match, flag that in response so that UI can
  tell user that results are fuzzy/completion.
- if there is a fitment filter do not do fuzzy searches and detect product type in query string
  so that no results come back for that bike
  - extend to other filters as make sense.


### Text Tokenization/Analysis ie. Punctuation ###

1. Change depunctuation to remove all punctuation and verify and regressions to punctuation-containing
searches.

2. Infer user has more precise text matching need when there is punctuation and handle like so:
https://www.elastic.co/guide/en/elasticsearch/reference/current/mixing-exact-search-with-stemming.html


### Short-Circuit/Precision Queries ###

1. Part description term/verbatim lookup ala. part number so that if you copy/paste a part
  description that part is the only to be returned.

2. Find terms that match in pre-emption order whitespace->lowercase->stemming; eg. if for query 's&s carb', we will
match 's&s' on brandName.whitespace or other whitespace-level fields, do not search on any wider-net analyzer fields
for 's&s'.
    
    - `adaptive headlight` - stemming matches "adapter" as equal to "adaptive" which is a misnomer
    - `veloce` and `velocity`

3. Use bayes classifier or some lookup using probabilistic that can give probability boost for a term to dimension.

### Search Term Exclusions/Normalizations ###

1. Non-Standard Publication Categories
    - only consider pub cats in Parts-Unlimited and Drag-Specialties
    - fixes DPI-1681

2. Also consider not including top-level brand-domain roots ie. Drag-Spec or Parts-Unlimited
   to avoid confusion with actual brands.

   - Example fix: `Drag headlight`

    matches non Drag brands since they belong under the drag-spec root pub cat



### Performance ###
    1. each filter option aggregation runs the full text query - twice
        - put all filter aggregations in the same query which will not change functionality
        - will stress test ES since size of each user request against ES is now much bigger
        - upside is that ES only has to run the full text query once when all aggregations
        feed off the same query results in the same request.
    
### Synonyms ###

1. Spotlight => light





partsearchalpha performs better with
    "FENDER RR FLS +4\" W/FRNCH" (exact part description)
        - in PSA returns just that part
        - in platform it is top result but many other results come back. (44k :))
    KNOWN - "red jacket"
        - in platform, top result in product is a blue jacket.
        - cause is not relevance/similarity-calc: jacket has a sku in red
        that is not visible, but the other skus are visible.

platform handles whereas partsearchalpha does not
    "three quarter helmet" - all three-quarter helmets
    "170/90-17" - all tires - matches on attribute value string.
    ["clock werk", "clockwerk"] - fuzziness enabled with stricter search matches no hits.
    "trx125 piston" - truly pistons for that model whereas psa returns a single brake shoe.

non-relevance differences
    "smith tear-off" plat has 8 parts <> psa has 96
        - parts in PSA results not in Platform results though do not have "smith" ANYWHERE
          in their product content.

oddsies
    "4x helmet icon"
        - in psa, alpinestars PANTS are returned ; no ICON or HELMETS in results ???
        - in platform, Icon Helmet liners are returned up top but also an AGV helmet
          since it has "Icon" in its attributes
    "slip stream"
        - in psa, no Slipstreamer branded parts are returned;
            - moreover, some parts are returned where they have have no apparent
              mention of either "slip" or "stream"
        - in platform, only Slipstreamer products are returned; HOWEVER,, 
            - parts in DIGI are NOT Active whereas in PartsSource they are, and these
              have in their part description "SLIP STREAM"
                examples:
                    1801-0754 - Crusher Muffler
                    1801-0757 - Crusher Muffler
                    1861-0982 - Crusher Exhaust Adapter
