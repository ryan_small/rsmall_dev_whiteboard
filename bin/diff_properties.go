package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

type PropertyComp struct {
	SubjectValue string
	PredicateValue string
}

func main() {
	subjectPropertiesFile := os.Args[1]
	predicatePropertiesFile := os.Args[2]

	var subjectProperties, subjectPropertiesFileErr = readPropertiesFile(subjectPropertiesFile)
	if subjectPropertiesFileErr != nil {
		log.Fatalf("First properties file could not be read: file=%s; error=$s", subjectPropertiesFile, subjectPropertiesFileErr)
	}
	var predicateProperties, predicatePropertiesFileErr = readPropertiesFile(predicatePropertiesFile)
	if predicatePropertiesFileErr != nil {
		log.Fatalf("Second properties file could not be read: file=%s; error=$s", predicatePropertiesFile, predicatePropertiesFileErr)
	}

	var subjectExclusiveProperties = make(map[string]string)
	var commonProperties = make(map[string]PropertyComp)
	var predicateExclusiveProperties = make(map[string]string)

	for subjectPropertyName := range subjectProperties {
		subjectPropertyValue := subjectProperties[subjectPropertyName]
		var predicateHasProperty bool = false

		for predicatePropertyName := range predicateProperties {
			predicatePropertyValue := predicateProperties[predicatePropertyName]

			if subjectPropertyName == predicatePropertyName {
				commonProperties[subjectPropertyName] = PropertyComp{ subjectPropertyValue, predicatePropertyValue }
				predicateHasProperty = true
				break
			}
		}

		if ! predicateHasProperty {
			subjectExclusiveProperties[subjectPropertyName] = subjectPropertyValue
		}
	}

	for predicatePropertyName := range predicateProperties {
		predicatePropertyValue := predicateProperties[predicatePropertyName]
		var subjectHasProperty bool = false

		for subjectPropertyName := range subjectProperties {
			if predicatePropertyName == subjectPropertyName {
				subjectHasProperty = true
				break
			}
		}

		if ! subjectHasProperty {
			predicateExclusiveProperties[predicatePropertyName] = predicatePropertyValue
		}
	}

	log.Printf("Common Properties")
	log.Print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
	for commonProperty := range commonProperties {
		var pair PropertyComp
		pair = commonProperties[commonProperty]
		valuesAreEqual := pair.SubjectValue == pair.PredicateValue
		log.Printf("%s = [%s vs. %s]; equal=%b", commonProperty, pair.SubjectValue, pair.PredicateValue, valuesAreEqual)
	}
	log.Print()

	log.Printf("Properties exclusive to first file")
	log.Print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
	for subjectExclusiveProperty := range subjectExclusiveProperties {
		log.Printf("%s = %s", subjectExclusiveProperty, subjectExclusiveProperties[subjectExclusiveProperty])
	}
	log.Print()

	log.Printf("Properties exclusive to second file")
	log.Print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ")
	for predicateExclusiveProperty := range predicateExclusiveProperties {
		log.Printf("%s = %s", predicateExclusiveProperty, predicateExclusiveProperties[predicateExclusiveProperty])
	}
}

func readPropertiesFile(filePath string) (map[string]string, error) {
	if file, openErr := os.Open(filePath); openErr != nil {
		return nil, openErr
	} else {
		defer file.Close()

		var properties = make(map[string]string)
		var lineReader = bufio.NewScanner(file)

		for lineReader.Scan() {
			propertyLineTokens := strings.Split(lineReader.Text(), "=")
			if len(propertyLineTokens) == 2 {
				properties[propertyLineTokens[0]] = propertyLineTokens[1]
			}
		}

		lineErr := lineReader.Err()

		if lineErr != nil {
			return nil, lineErr
		} else {
			return properties, nil
		}
	}
}