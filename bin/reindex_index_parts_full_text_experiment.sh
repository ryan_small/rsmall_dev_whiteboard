existing_index='parts_20180905_175452'

echo "Deleting experimental index"
../datastore_interaction/delete_indices -i parts_experimental_similarity --ignore-not-exists
echo ""

echo "Recreating experimental index"
curl -XPUT -H 'Content-Type: application/json' \
    http://localhost:9200/parts_experimental_similarity \
	-d @../product_service/domain/build/reports/test-output/parts_index_definition.json
echo ""

echo "Building experimental index from stock index"
../datastore_interaction/reindex_source_to_dest -source $existing_index --dest-index parts_experimental_similarity
echo ""

echo "Promoting experimental index to searched index"
../datastore_interaction/switch_aliased_index -p parts_experimental_similarity -r $existing_index -a parts
echo ""
