#!/bin/bash

SCRIPT_DIR=$(dirname $0)
pushd $SCRIPT_DIR > /dev/null

. conf/env
. conf/vars

version=$VERSION
reference_version=$REFERENCE_VERSION
products_index_name="products_$version"
products_reference_index_name="products_$reference_version"
parts_index_name="parts_$version"
parts_reference_index_name="parts_$reference_version"

DEFINITION_DIR=$WORKSPACE_PROJECTS_ROOT_DIR/product_service/domain/build/reports/test-output
DATASTORE_DIR=$WORKSPACE_PROJECTS_ROOT_DIR/dev/datastore_interaction

echo "Reindexing product and part version indicies: version=$version; reference_version=$reference_version"

echo "Deleting destination product index: name=[$products_index_name]"
$DATASTORE_DIR/delete_index --environment $ENVIRONMENT --index $products_index_name > /dev/null
echo ''
echo "Deleting destination part index: name=[$parts_index_name]"
$DATASTORE_DIR/delete_index --environment $ENVIRONMENT --index $parts_index_name > /dev/null
echo ''

echo "Creating destination product index: name=[$products_index_name]"
$DATASTORE_DIR/create_index --environment $ENVIRONMENT --index $products_index_name --definition-file $DEFINITION_DIR/products_index_definition.json
echo ''
echo "Creating destination part index: name=[$parts_index_name]"
$DATASTORE_DIR/create_index --environment $ENVIRONMENT --index $parts_index_name --definition-file $DEFINITION_DIR/parts_index_definition.json
echo ''

echo "Starting: copying reference product index to destination product index"
product_reindex_task=$($DATASTORE_DIR/reindex_source_to_destination --environment $ENVIRONMENT --source $products_reference_index_name --destination $products_index_name --wait-for-completion false | jq .task)
echo "Started: Product reindex task launched and can be monitored with this id: id=$product_reindex_task; waiting for result"
echo ''

echo "Starting: copying reference part index to destination part index"
part_reindex_task=$($DATASTORE_DIR/reindex_source_to_destination --environment $ENVIRONMENT --source $parts_reference_index_name --destination $parts_index_name --wait-for-completion false | jq .task)
echo "Started: Part reindex task launched and can be monitored with this id: id=$part_reindex_task; waiting for result"
echo ''

echo "Waiting for product reindex task to complete: id=$product_reindex_task"
$DATASTORE_DIR/wait_for_task_completion -i "$product_reindex_task"
echo ''
echo "Waiting for part reindex task to complete: id=$part_reindex_task"
$DATASTORE_DIR/wait_for_task_completion -i "$part_reindex_task"
echo ''
