#!/bin/bash

SCRIPT_DIR=$(dirname $0)
pushd $SCRIPT_DIR > /dev/null

DATASTORE_DIR=$WORKSPACE_PROJECTS_ROOT_DIR/dev/datastore_interaction

. conf/vars

environment='local'
version=$VERSION
reference_version=$REFERENCE_VERSION
alias='products'

while [[ $# > 0 ]]; do
	case $1 in
		-v|-version|--version)
			version=$2
			shift
			;;
		-r|-reference-version|--reference-version)
			reference_version=$2
			shift
			;;
        -e|-env|--environment)
			environment=$2
			shift
			;;
	esac
    shift
done

$DATASTORE_DIR/switch_aliased_index -a products -p products_$version -auto-remove
echo ''
$DATASTORE_DIR/switch_aliased_index -a parts -p parts_$version -auto-remove
echo ''

popd > /dev/null
