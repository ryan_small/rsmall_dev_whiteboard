
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

var applicationJson = "application/json"

type IndexRequestItem struct {
	Index string `json:"index"`
}
type ReindexRequest struct {
	Source IndexRequestItem `json:"source"`
	Dest IndexRequestItem `json:"dest"`
}

func main() {
	suffix := os.Args[1]

	if brandsIndex, err := getAliasedIndex("brands"); err != nil {
		os.Exit(1)
	} else {
		log.Printf("Brands index: %s", brandsIndex)
		reindexWithSuffix(brandsIndex, suffix)
	}
	if fitmentsIndex, err := getAliasedIndex("fitments"); err != nil {
		os.Exit(1)
	} else {
		log.Printf("Fitments index: %s", fitmentsIndex)
		reindexWithSuffix(fitmentsIndex, suffix)
	}
	if productsIndex, err := getAliasedIndex("products"); err != nil {
		os.Exit(1)
	} else {
		log.Printf("Products index: %s", productsIndex)
		reindexWithSuffix(productsIndex, suffix)
	}
	if partsIndex, err := getAliasedIndex("parts"); err != nil {
		os.Exit(1)
	} else {
		log.Printf("Parts index: %s", partsIndex)
		reindexWithSuffix(partsIndex, suffix)
	}
	if textClassifiedsIndex, err := getAliasedIndex("text_classifieds"); err != nil {
		os.Exit(1)
	} else {
		log.Printf("Text Classifieds index: %s", textClassifiedsIndex)
		reindexWithSuffix(textClassifiedsIndex, suffix)
	}
}

func getAliasedIndex(aliasName string) (string, error) {
	if response, requestErr := http.Get(
		"http://localhost:9200/_aliases",
	); requestErr != nil {
		log.Printf("Request failed: %s", requestErr)
		return "", requestErr
	} else {
		defer response.Body.Close()

		if responseBody, bodyReadErr := ioutil.ReadAll(response.Body); bodyReadErr != nil {
			log.Fatal("Reading response body failed: %s", bodyReadErr)
			return "", bodyReadErr
		} else {
			var responseObject map[string]interface{}
			if decodeErr := json.Unmarshal(responseBody, &responseObject); decodeErr != nil {
				log.Fatalf("Decoding response body text to JSON failed: %s", decodeErr)
				return "", decodeErr
			}

			for index, aliasSettings := range responseObject {
				if hasPathDeep(aliasSettings, fmt.Sprintf("aliases.%s", aliasName)) {
					return index, nil
				}
			}
			return "", nil
		}
	}
}

func hasPathDeep(obj interface{}, pathDotNotated string) bool {
	var tokens []string
	tokens = strings.Split(pathDotNotated, ".")
	switch obj.(type) {
		default:
			return false
		case map[string]interface{}:
			if subObj := obj.(map[string]interface{})[tokens[0]]; subObj != nil {
				if len(tokens) == 1 {
					return true
				} else {
					return hasPathDeep(subObj, strings.Join(tokens[1:], "."))
				}
			}
	}
	return false
}

func reindexWithSuffix(indexName string, reindexSuffix string) {
	requestObject := ReindexRequest{
		IndexRequestItem{indexName},
		IndexRequestItem{fmt.Sprintf("%s_%s", indexName, reindexSuffix)},
	}

	if requestBodyBuffer, err := json.Marshal(requestObject); err != nil {
		log.Fatal("Could not encode reindex request: %s", err)
		os.Exit(1)
	} else {
		log.Printf("Reindex request JSON: %s", requestBodyBuffer)
		if httpResponse, err := http.Post("http://localhost:9200/_reindex?wait_for_completion=true", applicationJson, bytes.NewReader(requestBodyBuffer)); err != nil {
			log.Fatal("Reindex request was rejected or failed: %s", err)
			os.Exit(1)
		} else {
			var indexResponse map[string]interface{}
			readRestJsonResponse(httpResponse, indexResponse)
			log.Printf("indexResponse: %s", indexResponse)
		}
	}
}

func readRestJsonResponse(httpResponse *http.Response, obj interface{}) {
	defer httpResponse.Body.Close()
	if responseBody, err := ioutil.ReadAll(httpResponse.Body); err != nil {
		log.Fatal("Reading REST response failed: %s", err)
		os.Exit(1)
	} else {
		json.Unmarshal(responseBody, &obj)
	}
}
